<#
.SYNOPSIS
    Initialize an AWS account with networking building blocks without manually creating these in each activated region.
.DESCRIPTION
    This scripts initializes an AWS account with AWS networking building blocks in each region that incur no charges. The objective is to get familiar with AWSPowershell cmdlets by mimicking a tiny functionality sets of Terraform or CloudFormation.
    1. A new VPC with unique CIDR range that does not overlap with other VPCs.
    2. One public subnet in first AZ and an internet gateway.
    3. A private subnet in each AZ.
    4. A security group named allow-all-in-out that allows all inbound and outbound traffic, to be used in a private subnet resources.
    5. A public subnet default route.
.PARAMETER ConfigFile
    Configuration parameter file. Default is .\config-init-infra.json if not specified.
.PARAMETER IsDebug
    For debugging purpose.
.PARAMETER IsProd
    Actually create AWS resources.
.PARAMETER CleanUp
    [Caution] Cleanup all AWS VPC resources in regions other than excluded regions.
.EXAMPLE
    .\Initialize-AWSInfrastructure.ps1 -IsProd
    Create AWS networking building blocks.
.EXAMPLE
    [Caution] .\Initialize-AWSInfrastructure.ps1 -CleanUp
    [Caution] Cleanup all AWS VPC resources in regions other than excluded regions..INPUTS
.OUTPUTS
    FileSystem.Object
        error.log
        Error log generated when script initialization failed.
    FileSystem.Object
        Initialize-AWSInfrastructure.log
        Execution log files.
.NOTES
    Script name : Initialize-AWSInfrastructure.ps1
    Disclaimer  : This script is provided as-is without guarantee. Please read the script to understand what it does prior to using it.
#>

[CmdletBinding()]
Param
(
    [Parameter(Mandatory=$false)]
    [ValidateNotNullOrEmpty()]
    [string]$ConfigFile,

    [Parameter(Mandatory=$false)]
    [switch]$IsProd = $false,

    [Parameter(Mandatory=$false)]
    [switch]$CleanUp = $false,

    [Parameter(Mandatory=$false)]
    [switch]$IsDebug
)

#Requires -Version 5.1
#Requires -PSEdition Desktop
#Requires -Modules AWSPowershell

## Common variables
$ErrorActionPreference = "Stop"
$global:PsScriptRoot = $PsScriptRoot
$BootStrapErrorLog = "$global:PsScriptRoot\error.log"
$global:LogPath = "$global:PsScriptRoot\logs\Initialize-AWSInfrastructure.log"
$global:IsDebug = $IsDebug

if (-Not $ConfigFile) {
    $ConfigFile = "$global:PsScriptRoot\config-init-infra.json"
}

try {
    $global:config = Get-Content -Path $ConfigFile -Raw | ConvertFrom-Json
}
catch {
    "$(Get-Date -Format "yyyy-MM-dd HH:mm:ss") | Unable to get $ConfigFile config file." | Out-File -FilePath $BootStrapErrorLog -Append
    Exit
}

## AWS variables
$global:AwsRegion = $global:config.aws.default_region
[string[]]$global:CurrentCidrs = @()

if (Get-Module -Name AWSPowershell) {
    Write-Host "AWSPowershell module is already loaded."
    $global:config.main.modules = $global:config.main.modules
}

foreach ($Module in ( $global:config.main.modules | Where-Object {$_.name -NotMatch "AWSPowershell"} ) ) {
    Write-Host "Importing $($Module.name) module..."
    $Module.path = "$global:PsScriptRoot\$($Module.path)"

    try {
        if ($Module.Version) {
            Import-Module $Module.path -Version $Module.Version -Force -DisableNameChecking
        }
        else {
            Import-Module $Module.path -Force -DisableNameChecking
        }
    }
    catch {
        "$(Get-Date -Format "yyyy-MM-dd HH:mm:ss") | Unable to import $($Module.path) module." | Out-File -FilePath $BootStrapErrorLog -Append
        Exit
    }
}

Write-Log " "
Reset-Log -FileName $global:LogPath -FileSize 1024kb -LogCount 3
Write-Log "Starting script as $env:USERNAME on $global:PsScriptRoot, logging to $global:LogPath..."
if ($global:IsDebug) { Write-Log -Level Warn "Debug is enabled."}

try {
    Write-Log "Setting default AWS region to $($global:AwsRegion)..."
    Set-DefaultAWSRegion -Region $global:AwsRegion
}
catch {
    Get-Exception $_ "Unable to set default AWS region."
    Exit
}

Write-Log (ConvertTo-FlatObject -InputObject $global:config | Out-String)

<#
Data
- CIDR: 172.16.0.0/12
- Region: 172.[16-31].0.0/16
- AZ: 172.[16-31].[1-255].0/24

Process
1. Get all regions - done
2. Create a new VPC - done
3. Create one public subnet in first AZ and one subnet for each AZ in new VPC - done
4. Create internet gateway and attach it to new VPC - done
5. Add internet route via igw to default route table - done
6. Create VPC peering.
7. Modify route table for cross region connectivity
8. Create allow-all-in-out SG - done
#>

$ExcludedRegions = $global:config.aws.excluded_regions.region
$IncludedRegions = $global:config.aws.included_regions.region

Write-Log "Getting all AWS regions..."
try {
    $AllAwsRegions = (Get-AwsRegion).Region
}
catch {
    Get-Exception $_ "Unable to get list of AWS regions."
    Exit
}

if (-Not $IncludedRegions) {
    $FilteredAwsRegions = Compare-Object -Reference $ExcludedRegions -DifferenceObject $AllAwsRegions -PassThru
}
else {
    Write-Log "Included regions values exist, only processing included regions."
    $FilteredAwsRegions = $IncludedRegions
}

if ($CleanUp) {
    CleanUp -FilteredAwsRegions $FilteredAwsRegions
}

if ($global:config.aws.vpc.current_cidrs) {
    $global:CurrentCidrs = $global:config.aws.vpc.current_cidrs.cidr
}
else {
    Write-Log "Getting current CIDRs..."
    $global:CurrentCidrs = Get-CurrentCidrs -Regions $AllAwsRegions -ExcludedRegions $ExcludedRegions
}

Write-Log "Current CIDRs: $($global:CurrentCidrs)"
Write-Log "Enumerating potential CIDRs..."
$RegionVpcCidrs = Get-CIDR -BaseCidr $global:config.aws.vpc.account_cidr -ChildCidr $global:config.aws.vpc.region_vpc_cidr_network_bits

if ($IsProd) {
    $CreatedVpcs = Create-NewVpcs -Regions $FilteredAwsRegions -RegionVpcCidrs $RegionVpcCidrs
    foreach ($CreatedVpc in $CreatedVpcs) {
        Write-Log "Processing $($CreatedVpc.Region) region..."
        $CreatedIgw = CreateInternetGateway -CreatedVpc $CreatedVpc
        $CreatedSg = CreateSecurityGroup -CreatedVpc $CreatedVpc
        $CreatedSubnets = CreateSubnet -CreatedVpc $CreatedVpc
        $AddDefaultRouteResult = AddRouteTableDefaultRoute -Igw $CreatedIgw -CreatedVpc $CreatedVpc
        #CreateVpcPeering -CreatedVpc $CreatedVpc
    }
}

Write-Log "Script ends."