# Function template
function zzTemplate {
    <#
    .SYNOPSIS
    .DESCRIPTION
    .PARAMETER Message
    .EXAMPLE
    .INPUTS
    .OUTPUTS
    #>
}

function Write-Log {
    <#
    .SYNOPSIS
        Writes log to a file with timestamp.
    .DESCRIPTION
    .PARAMETER Message
        Specify log message to write to log file.
    .PARAMETER Path
        Log file path. If no log file specified, it will write to current directory with timestamp down to the current hour. For example: .\Log_20190324_09.log
    .PARAMETER Level
        Specify log level: Error, Warn or Info. If no value specified, it defaults to Info.
    .PARAMETER NoClobber
        If specified, do not write to the log file if the file already exists.
    .EXAMPLE
        Write-Log -Message "This is a test log message" -Level Warn
        Write "This is a test log message" to current directory with warning level.
    .INPUTS
        System.String
            String of messages.
    .OUTPUTS
        System.String
            String to log file and console.
    #>

    [CmdletBinding()]
    Param (
        [Parameter(Mandatory=$true,Position=0)]
        [ValidateNotNullOrEmpty()]
        [string]$Message,

        [Parameter(Mandatory=$false)]
        [string]$Path,
        
        [Parameter(Mandatory=$false)]
        [ValidateSet("Error","Warn","Info")]
        [string]$Level="Info",
        
        [Parameter(Mandatory=$false)]
        [switch]$NoClobber
    )

    Begin {
        $VerbosePreference = 'Continue'

        if (-Not $Path) {
            #$Path = "$global:PsScriptRoot\logs\Log_" + (Get-Date -Format "yyyyMMdd_HH") + ".log"
            $Path = $global:LogPath
        }
    }
    Process {        
        if ((Test-Path $Path) -AND $NoClobber) {
            Write-Error "Log file $Path already exists, and you specified NoClobber. Either delete the file or specify a different name."
            Return
        }

        elseif (!(Test-Path $Path)) {
            Write-Verbose "Creating $Path."
            New-Item $Path -Force -ItemType File
        }

        $FormattedDate = (Get-Date).ToUniversalTime().ToString("yyyy-MM-dd HH:mm:ssZ")

        switch ($Level) {
            'ERROR' {
                Write-Host  "$FormattedDate [ERROR] $($(Get-PsCallStack)[1].Command) - $Message" -ForegroundColor Magenta
                $LevelText = 'ERROR'
            }
            'WARN' {
                Write-Host  "$FormattedDate [WARN] $($(Get-PsCallStack)[1].Command) - $Message" -ForegroundColor Yellow
                $LevelText = 'WARNING'
            }
            'INFO' {
                Write-Host  "$FormattedDate [INFO] $($(Get-PsCallStack)[1].Command) - $Message" -ForegroundColor Green
                $LevelText = 'INFO'
            }   
        }
        
        "$FormattedDate [$LevelText] $($(Get-PsCallStack)[1].Command) - $Message" | Out-File -FilePath $Path -Append
    }
    End {
        return $Path | Out-Null
    }
}

function Get-Exception {   
    <#
    .SYNOPSIS
        Catch an exception and log it to a file.
    .DESCRIPTION
    .PARAMETER ExceptionObject
        The exception object.
    .PARAMETER ForceExit
        Terminate the running script.
    .PARAMETER Message
        Custom message written to the log file.
    .EXAMPLE
        Get-Exception -ExceptionObject $_ -Message "This is an exception" -ForceExit
        Capture an exception, log it with a message "This is an exception" and quit the program.
    .INPUTS
    .OUTPUTS
        System.String
            String to log file and console.
    #>

    [CmdletBinding()]
    Param (
        [Parameter(Mandatory=$true, Position=0)]
        [ValidateNotNullOrEmpty()]
        [Object[]]$ExceptionObject,

        [Parameter(Mandatory=$false, Position=1)]
        [string]$Message,

        [Parameter(Mandatory=$false, Position=2)]
        [ValidateNotNullOrEmpty()]
        #[ValidateSet($true,$false)]
        [switch]$ForceExit,
        
        [Parameter(Mandatory=$false, Position=3)]
        [string]$Function
    )

    Process {
        if ($Message) {
            # Add period to the custom error message if none exists.
            if ($Message.Substring($Message.Length - 1) -ne '.') {
                $Message = $Message + '.'
            }
    
            Write-Log -Level Error "$Message Exception on $($ExceptionObject.Exception.ItemName) with error: $($ExceptionObject.Exception.Message)"
            Write-Log -Level Error "Code trace: $($ExceptionObject.InvocationInfo.PositionMessage)"
        }
        else {
            Write-Log -Level Error "Exception on $($ExceptionObject.Exception.ItemName) with error: $($ExceptionObject.Exception.Message)"
            Write-Log -Level Error "Code trace: $($ExceptionObject.InvocationInfo.PositionMessage)"
        }
        if ($ForceExit) {
            Write-Log -Level Error "Terminating program."
            Exit
        }
    }
}

function Reset-Log { 
    Param (
        [Parameter(Mandatory=$true,
        ValueFromPipelineByPropertyName=$true)]
        [Alias("LogFile")]
        [string]$FileName,

        [Parameter(Mandatory=$false)]
        [Alias("LogSize")]
        [int64]$FileSize = 10240kb,

        [Parameter(Mandatory=$false)]
        [Alias("LogFileCount")]
        [int] $LogCount = 5
    )
     
    $LogRollStatus = $true
    Write-Log -Level Info "Starting log rotation function with log file $FileName, threshold size $FileSize, file count $LogCount..."

    if(Test-Path $FileName) {
        Write-Log -Level Info "Log maintenance: Found existing log file $FileName"
        $file = Get-ChildItem $FileName
        if((($file).length) -ige $FileSize) {
            Write-Log -Level Info "Log maintenance: Log file $FileName is greater than $($file.length), rolling log file..."
            $FileDir = $file.Directory
            $fn = $file.name
            $files = Get-ChildItem $FileDir | Where-Object {$_.name -like "$fn*"} | Sort-Object LastWriteTime
            $FileFullName = $file.FullName

            for ($i = ($files.count); $i -gt 0; $i--) {
                $files = Get-ChildItem $FileDir | Where-Object {$_.name -like "$fn*"} | Sort-Object LastWriteTime
                $OperatingFile = $files | Where-Object {($_.name).trim($fn) -eq $i}
                if ($OperatingFile) {
                    $OperatingFileNumber = ($files | Where-Object {($_.name).trim($fn) -eq $i}).name.trim($fn)
                }
                else {
                    $OperatingFileNumber = $null
                }
 
                if(($OperatingFileNumber -eq $null) -and ($i -ne 1) -and ($i -lt $logcount)) {
                    $OperatingFileNumber = $i
                    $NewFileName = "$FileFullName.$OperatingFileNumber"
                    $OperatingFile = $files | Where-Object {($_.name).trim($fn) -eq ($i-1)}
                    Write-Log -Level Info "Moving $($OperatingFile.FullName) to $NewFileName"
                    try {
                        Move-Item ($OperatingFile.FullName) -Destination $NewFileName -Force
                    }
                    catch {
                        Get-Exception -ExceptionObject $_ -Message "Unable to move $($OperatingFile.FullName) to $NewFileName."
                    }
                }
                elseif ($i -ge $logcount) {
                    if ($OperatingFileNumber -eq $null) {
                        $OperatingFileNumber = $i - 1
                        $OperatingFile = $files | Where-Object {($_.name).trim($fn) -eq $OperatingFileNumber}
                    }
                    Write-Log -Level Info "Deleting $($OperatingFile.FullName)"
                    try {
                        Remove-Item $($OperatingFile.FullName) -Force 
                    }
                    catch {
                        Get-Exception -ExceptionObject $_ -Message "Unable to delete $($OperatingFile.FullName)."
                    }
                }
                elseif($i -eq 1) {
                    $OperatingFileNumber = 1
                    $NewFileName = "$FileFullName.$OperatingFileNumber"
                    Write-Log -Level Info "Moving to $FileFullName to $NewFileName"
                    try
                    {
                        Move-Item $FileFullName -Destination $NewFileName -Force 
                    }
                    catch
                    {
                        Get-Exception -ExceptionObject $_ -Message "Unable to move $FileFullName) to $NewFileName."
                    }
                }
                else {
                    $OperatingFileNumber = $i +1
                    $NewFileName = "$FileFullName.$OperatingFileNumber"
                    $OperatingFile = $files | Where-Object {($_.name).trim($fn) -eq ($i-1)}
                    Write-Log -Level Info "Moving to $($OperatingFile.FullName) to $NewFileName"
                    try {
                        Move-Item $($OperatingFile.FullName) -Destination $NewFileName -Force
                    }
                    catch {
                        Get-Exception -ExceptionObject $_ -Message "Unable to move $($OperatingFile.FileFullName) to $NewFileName."
                    }   
                }     
            } 
        } 
        else {
            Write-Log -Level Info "Log maintenance: Log file $FileName is smaller $($file.length) than log rotation threshold $FileSize "
            $LogRollStatus = $false
        } 
    }
    else { 
        Write-Log -Level Info "Log maintenance: Unable to access log file $FileName"
        $LogRollStatus = $false 
    } 
}

function Get-PublicIpAddress {
    <#
    .SYNOPSIS
        Determine current public IP address by querying publicly available HTTP endpoint.
    .DESCRIPTION
    .PARAMETER LookupUri
        System.String
            Specify lookup Uri that returns public IP address in plain text format. Default is http://ipecho.net/plain
    .EXAMPLE
        Get-PublicIpAddress
        Get current public IP address.
    .INPUTS
    .OUTPUTS
        System.String
            This cmdlet returns a string object of IP address.
    #>

    [CmdletBinding()]
    Param (
        [Parameter(Mandatory=$false)]
        [string]$LookupUri = 'http://ipecho.net/plain'
    )

    try {
        $CurrentPublicIPAddress = (Invoke-WebRequest -Uri $LookupUri).Content
    }
    catch {
        Get-Exception -ExceptionObject $_ -Message "Unable to get current public IP address from $LookupUri."
    }
    
    if ($CurrentPublicIPAddress) {
        Write-Log "Current public IP address: $CurrentPublicIpAddress."
    }
    return $CurrentPublicIPAddress
}

function New-AmazonEc2ModelFilter {
    [CmdletBinding()]
    Param (
        [Parameter(Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        [string]$Name,

        [Parameter(Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        [string[]]$Values
    )

    $Filter = New-Object -TypeName Amazon.Ec2.Model.Filter
    $Filter.Name = $Name
    $Filter.Values = $Values
    return $Filter
}

function Create-AwsVpnCustomerGateway {
    <#
    .SYNOPSIS
    Creates Ec2 VPN gateway by calling the Amazon Elastic Compute Cloud CreateCustomerGateway API operation. 
    .DESCRIPTION
    .PARAMETER PublicIpAddress
        System.String
            IPv4 public IP address.
    .PARAMETER BgpAsn
        System.Int32
            BGP ASN.
    .PARAMETER GatewayType
        Amazon.EC2.GatewayType
            VPN gateway type: ipsec.1
    .EXAMPLE
        Create-AwsVpnCustomerGateway
    .INPUTS
    .OUTPUTS
        Amazon.EC2.Model.CustomerGateway
            This cmdlet returns a CustomerGateway object.
    #>

    Param (
        [Parameter(Mandatory=$false)]
        [string]$PublicIpAddress,

        [Parameter(Mandatory=$false)]
        [string]$BgpAsn,

        [Parameter(Mandatory=$false)]
        [string]$GatewayType
    )

    if (-Not $PublicIpAddress) { $PublicIpAddress = $global:PublicIpAddress }
    if (-Not $BgpAsn) { $BgpAsn = $global:AwsEc2CustomerGatewayBgpAsn }
    if (-Not $GatewayType) { $GatewayType = $global:AwsEc2CustomerGatewayType }

    Write-Log "Creating $PublicIpAddress customer VPN gateway with ASN $BgpAsn and type $GatewayType..."
    try {
        $NewCustomerGateway = New-EC2CustomerGateway -Type $GatewayType -PublicIp $PublicIpAddress -BgpAsn $BgpAsn
    }
    catch {
        Get-Exception -ExceptionObject $_ -Message "Unable to create $PublicIpAddress customer VPN gateway."
    }

    if ($NewCustomerGateway) {
        Write-Log "Created customer VPN gateway: $($NewCustomerGateway.CustomerGatewayId)"
        return $NewCustomerGateway
    }
}

function Delete-Ec2VpnConnection {
    <#
    .SYNOPSIS
    .DESCRIPTION
    .PARAMETER Message
    .EXAMPLE
    .INPUTS
    .OUTPUTS
    #>

    $Filter = New-AmazonEc2ModelFilter -Name "state" -Values @("available")
    foreach ($VpnConnection in (Get-Ec2VpnConnection -Filter $Filter)) {
        Write-Log -Level Warn "Removing $($VpnConnection.VpnConnectionId)..."
        try {
            Remove-Ec2VpnConnection -VpnConnectionId $VpnConnection.VpnConnectionId -Force
        }
        catch {
            Get-Exception -ExceptionObject $_ -Message "Unable to remove $($VpnConnection.VpnConnectionId)."
        }
    }
}

function Create-Ec2VpnConnection {
    <#
    .SYNOPSIS
    .DESCRIPTION
    .PARAMETER Message
    .EXAMPLE
    .INPUTS
    .OUTPUTS
    #>

    Param (
        [Parameter(Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        [string]$CustomerGatewayId,

        [Parameter(Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        [string]$VpnGatewayId,

        [Parameter(Mandatory=$false)]
        [ValidateNotNullOrEmpty()]
        $VpnTunnelOptionsSpec
    )

    Write-Log "Creating a new vpn connection using $CustomerGatewayId between $VpnGatewayId..."
    try {
        $VpnConnection = New-Ec2VpnConnection -Type ipsec.1 -CustomerGatewayId $CustomerGatewayId -VpnGatewayId $VpnGatewayId
    }
    catch {
        Get-Exception -ExceptionObject $_ -Message "Unable to create new VPN connection."
    }
    return $VpnConnection
}

function Get-RandomCharacters ($length, $characters) {
    <#
    .SYNOPSIS
    .DESCRIPTION
    .PARAMETER Message
    .EXAMPLE
    .INPUTS
    .OUTPUTS
    #>

    $random = 1..$length | ForEach-Object { Get-Random -Maximum $characters.length } 
    return [String]$characters[$random]
}

function Scramble-String([string]$inputString) {
    <#
    .SYNOPSIS
    .DESCRIPTION
    .PARAMETER Message
    .EXAMPLE
    .INPUTS
    .OUTPUTS
    #>

    $characterArray = $inputString.ToCharArray()   
    $scrambledStringArray = $characterArray | Get-Random -Count $characterArray.Length     
    $outputString = -Join $scrambledStringArray
    return $outputString 
}

function Fix-AwsVpnConnection {
    <#
    .SYNOPSIS
    .DESCRIPTION
    .PARAMETER Message
    .EXAMPLE
    .INPUTS
    .OUTPUTS
    #>

    if ($global:PublicIpAddress) {
        $NewCustomerGateway = Create-AwsVpnCustomerGateway
    }
    else {
        Get-Exception -ExceptionObject $_ -Message "Unable to create new customer gateway, exiting now." -ForceExit
    }

    if ($NewCustomerGateway) {
        Write-Log "Retrieving VPN gateway ID..."
        $Filter = New-AmazonEc2ModelFilter -Name "state" -Values @("available")
        try {
            $VpnGatewayId = (Get-Ec2VpnGateway -Filter $Filter | Select-Object -First 1).VpnGatewayId
        }
        catch {
            Get-Exception -ExceptionObject $_ -Message "Unable to retrieve VPN gateway ID, exiting now." -ForceExit
        }

        Delete-Ec2VpnConnection
        $rNewEc2VpnConnection = Create-Ec2VpnConnection -CustomerGatewayId $NewCustomerGateway.CustomerGatewayId -VpnGatewayId $VpnGatewayId
    }
    else {
        Get-Exception -ExceptionObject $_ -Message "New customer gateway failed to be created, exiting now." -ForceExit
    }

    if ($rNewEc2VpnConnection) {
        return $rNewEc2VpnConnection
    }
    else {
        Write-Log -Level Error "New Vpn connection creation failed."
    }
}

function Check-AwsVpnTunnel {
    <#
    .SYNOPSIS
    .DESCRIPTION
    .PARAMETER Message
    .EXAMPLE
    .INPUTS
    .OUTPUTS
    #>

    $IsVpnOk = $false
    $Filter = New-AmazonEc2ModelFilter -Name "state" -Values @("available", "pending")  
    Write-Log "Checking existing VPN connections..."
    try {
        $EC2VpnConnections = Get-EC2VpnConnection -Filter $Filter
    }
    catch {
        Get-Exception -ExceptionObject $_ -Message "Unable to retrieve site-to-site VPN connections." -ForceExit
    }

    if ($EC2VpnConnections) {
        foreach ($Connection in $EC2VpnConnections) {
            foreach ($Tunnel in $Connection.VgwTelemetry) {
                Write-Log "Checking $($Connection.VpnConnectionId) to $($Tunnel.OutsideIpAddress) VPN tunnel..."
                if ($Tunnel.Status -match "UP"-and -Not $global:IsDebug) {
                    Write-Log "$($Connection.VpnConnectionId) to $($Tunnel.OutsideIpAddress) is $($Tunnel.Status)!"
                    $IsVpnOk = $true
                    Write-Log "At least one VPN tunnel is up, skipping other checks."
                    Break
                }
                else {
                    Write-Log "$($Connection.VpnConnectionId) to $($Tunnel.OutsideIpAddress) is $($Tunnel.Status)!"
                    $IsVpnOk = $false
    
                    Write-Log "Getting list of customer gateways..."
                    try {
                        $CxGatewayIp = (Get-Ec2CustomerGateway -Filter $Filter).IpAddress
                    }
                    catch {
                        Get-Exception -ExceptionObject $_ -Message "Unable to get list of customer gateways."
                    }
    
                    if ( $CxGatewayIp -notcontains $global:PublicIpAddress -or $IsDebug ) {
                        Write-Log -Level Warn "Router public IP does not match any Cx gateways."
                        $rFixAwsVpnConnection = Fix-AwsVpnConnection
                        Break
                    }
                    else {
                        Write-Log "Router public IP match one of Cx gateways."
                        Break
                    }
                }
            }
        }
        if (-Not $IsVpnOk) {
            #Send notification when router IP has not changed but tunnel is down.
        }
    }
    else {
        Write-Log -Level Warn "No working VPN connection, let's create one."
        $rFixAwsVpnConnection = Fix-AwsVpnConnection
        Start-Sleep 5
    }

    if ($rFixAwsVpnConnection) {
        return $rFixAwsVpnConnection
    }
}

function Ignore-SSLValidation {
    Write-Log -Level Warn "Setting to ignore SSL certificate validation..."
    Add-Type @"
    using System.Net;
    using System.Security.Cryptography.X509Certificates;
    public class TrustAllCertsPolicy : ICertificatePolicy {
        public bool CheckValidationResult(
            ServicePoint srvPoint, X509Certificate certificate,
            WebRequest request, int certificateProblem) {
            return true;
        }
    }
"@
    $AllProtocols = [System.Net.SecurityProtocolType]'Ssl3,Tls,Tls11,Tls12'
    [System.Net.ServicePointManager]::SecurityProtocol = $AllProtocols
    [System.Net.ServicePointManager]::CertificatePolicy = New-Object TrustAllCertsPolicy
}

function CleanUp-CustomerVpnGateway {
    try {
        $CustomerVpnGateways = (Get-Ec2CustomerGateway | Where-Object {$_.State -NotMatch "deleted"}).CustomerGatewayId
        $ActiveCustomerVpnGateway = (Get-Ec2VpnConnection | Where-Object {$_.State -NotMatch "deleted"}).CustomerGatewayId
    }
    catch {
        Get-Exception -ExceptionObject $_ -Message "Unable to retrieve list of customer gateways."
        Break
    }

    if ($ActiveCustomerVpnGateway -and $CustomerVpnGateways) {
        $Diff = Compare-Object -ReferenceObject $ActiveCustomerVpnGateway -DifferenceObject $CustomerVpnGateways -PassThru
        if ($Diff) {
            foreach ($CxVpnGw in $Diff) {
                Write-Log -Level Warn "Removing $CxVpnGw customer gateway..."
                try {
                    Remove-Ec2CustomerGateway -CustomerGatewayId $CxVpnGw -Force
                }
                catch {
                    Get-Exception -ExceptionObject $_ -Message "Unable to remove $CxVpnGw customer gateway."
                }
            }
        }
    }
}

Function ConvertTo-FlatObject {
    <#
    .SYNOPSIS
        Flatten an object to simplify discovery of data

    .DESCRIPTION
        Flatten an object.  This function will take an object, and flatten the properties using their full path into a single object with one layer of properties.

        You can use this to flatten XML, JSON, and other arbitrary objects.

        This can simplify initial exploration and discovery of data returned by APIs, interfaces, and other technologies.

        NOTE:
            Use tools like Get-Member, Select-Object, and Show-Object to further explore objects.
            This function does not handle certain data types well.  It was original designed to expand XML and JSON.

    .PARAMETER InputObject
        Object to flatten

    .PARAMETER Exclude
        Exclude any nodes in this list.  Accepts wildcards.

        Example:
            -Exclude price, title

    .PARAMETER ExcludeDefault
        Exclude default properties for sub objects.  True by default.

        This simplifies views of many objects (e.g. XML) but may exclude data for others (e.g. if flattening a process, ProcessThread properties will be excluded)

    .PARAMETER Include
        Include only leaves in this list.  Accepts wildcards.

        Example:
            -Include Author, Title

    .PARAMETER Value
        Include only leaves with values like these arguments.  Accepts wildcards.

    .PARAMETER MaxDepth
        Stop recursion at this depth.

    .INPUTS
        Any object

    .OUTPUTS
        System.Management.Automation.PSCustomObject

    .EXAMPLE

        #Pull unanswered PowerShell questions from StackExchange, Flatten the data to date a feel for the schema
        Invoke-RestMethod "https://api.stackexchange.com/2.0/questions/unanswered?order=desc&sort=activity&tagged=powershell&pagesize=10&site=stackoverflow" |
            ConvertTo-FlatObject -Include Title, Link, View_Count

            $object.items[0].owner.link : http://stackoverflow.com/users/1946412/julealgon
            $object.items[0].view_count : 7
            $object.items[0].link       : http://stackoverflow.com/questions/26910789/is-it-possible-to-reuse-a-param-block-across-multiple-functions
            $object.items[0].title      : Is it possible to reuse a &#39;param&#39; block across multiple functions?
            $object.items[1].owner.link : http://stackoverflow.com/users/4248278/nitin-tyagi
            $object.items[1].view_count : 8
            $object.items[1].link       : http://stackoverflow.com/questions/26909879/use-powershell-to-retreive-activated-features-for-sharepoint-2010
            $object.items[1].title      : Use powershell to retreive Activated features for sharepoint 2010
            ...

    .EXAMPLE

        #Set up some XML to work with
        $object = [xml]'
            <catalog>
               <book id="bk101">
                  <author>Gambardella, Matthew</author>
                  <title>XML Developers Guide</title>
                  <genre>Computer</genre>
                  <price>44.95</price>
               </book>
               <book id="bk102">
                  <author>Ralls, Kim</author>
                  <title>Midnight Rain</title>
                  <genre>Fantasy</genre>
                  <price>5.95</price>
               </book>
            </catalog>'

        #Call the flatten command against this XML
            ConvertTo-FlatObject $object -Include Author, Title, Price

            #Result is a flattened object with the full path to the node, using $object as the root.
            #Only leaf properties we specified are included (author,title,price)

                $object.catalog.book[0].author : Gambardella, Matthew
                $object.catalog.book[0].title  : XML Developers Guide
                $object.catalog.book[0].price  : 44.95
                $object.catalog.book[1].author : Ralls, Kim
                $object.catalog.book[1].title  : Midnight Rain
                $object.catalog.book[1].price  : 5.95

        #Invoking the property names should return their data if the orginal object is in $object:
            $object.catalog.book[1].price
                5.95

            $object.catalog.book[0].title
                XML Developers Guide

    .EXAMPLE

        #Set up some XML to work with
            [xml]'<catalog>
               <book id="bk101">
                  <author>Gambardella, Matthew</author>
                  <title>XML Developers Guide</title>
                  <genre>Computer</genre>
                  <price>44.95</price>
               </book>
               <book id="bk102">
                  <author>Ralls, Kim</author>
                  <title>Midnight Rain</title>
                  <genre>Fantasy</genre>
                  <price>5.95</price>
               </book>
            </catalog>' |
                ConvertTo-FlatObject -exclude price, title, id

        Result is a flattened object with the full path to the node, using XML as the root.  Price and title are excluded.

            $Object.catalog                : catalog
            $Object.catalog.book           : {book, book}
            $object.catalog.book[0].author : Gambardella, Matthew
            $object.catalog.book[0].genre  : Computer
            $object.catalog.book[1].author : Ralls, Kim
            $object.catalog.book[1].genre  : Fantasy

    .EXAMPLE
        #Set up some XML to work with
            [xml]'<catalog>
               <book id="bk101">
                  <author>Gambardella, Matthew</author>
                  <title>XML Developers Guide</title>
                  <genre>Computer</genre>
                  <price>44.95</price>
               </book>
               <book id="bk102">
                  <author>Ralls, Kim</author>
                  <title>Midnight Rain</title>
                  <genre>Fantasy</genre>
                  <price>5.95</price>
               </book>
            </catalog>' |
                ConvertTo-FlatObject -Value XML*, Fantasy

        Result is a flattened object filtered by leaves that matched XML* or Fantasy

            $Object.catalog.book[0].title : XML Developers Guide
            $Object.catalog.book[1].genre : Fantasy

    .EXAMPLE
        #Get a single process with all props, flatten this object.  Don't exclude default properties
        Get-Process | select -first 1 -skip 10 -Property * | ConvertTo-FlatObject -ExcludeDefault $false

        #NOTE - There will likely be bugs for certain complex objects like this.
                For example, $Object.StartInfo.Verbs.SyncRoot.SyncRoot... will loop until we hit MaxDepth. (Note: SyncRoot is now addressed individually)

    .NOTES
        I have trouble with algorithms.  If you have a better way to handle this, please let me know!

    .FUNCTIONALITY
        General Command
    #>
    [cmdletbinding()]
    param(

        [parameter( Mandatory = $True,
                    ValueFromPipeline = $True)]
        [PSObject[]]$InputObject,

        [string[]]$Exclude = "",

        [bool]$ExcludeDefault = $True,

        [string[]]$Include = $null,

        [string[]]$Value = $null,

        [int]$MaxDepth = 10
    )
    Begin
    {
        #region FUNCTIONS

            #Before adding a property, verify that it matches a Like comparison to strings in $Include...
            Function IsIn-Include {
                param($prop)
                if(-not $Include) {$True}
                else {
                    foreach($Inc in $Include)
                    {
                        if($Prop -like $Inc)
                        {
                            $True
                        }
                    }
                }
            }

            #Before adding a value, verify that it matches a Like comparison to strings in $Value...
            Function IsIn-Value {
                param($val)
                if(-not $Value) {$True}
                else {
                    foreach($string in $Value)
                    {
                        if($val -like $string)
                        {
                            $True
                        }
                    }
                }
            }

            Function Get-Exclude {
                [cmdletbinding()]
                param($obj)

                #Exclude default props if specified, and anything the user specified.  Thanks to Jaykul for the hint on [type]!
                    if($ExcludeDefault)
                    {
                        Try
                        {
                            $DefaultTypeProps = @( $obj.gettype().GetProperties() | Select -ExpandProperty Name -ErrorAction Stop )
                            if($DefaultTypeProps.count -gt 0)
                            {
                                Write-Verbose "Excluding default properties for $($obj.gettype().Fullname):`n$($DefaultTypeProps | Out-String)"
                            }
                        }
                        Catch
                        {
                            Write-Verbose "Failed to extract properties from $($obj.gettype().Fullname): $_"
                            $DefaultTypeProps = @()
                        }
                    }

                    @( $Exclude + $DefaultTypeProps ) | Select -Unique
            }

            #Function to recurse the Object, add properties to object
            Function Recurse-Object {
                [cmdletbinding()]
                param(
                    $Object,
                    [string[]]$path = '$Object',
                    [psobject]$Output,
                    $depth = 0
                )
                # Handle initial call
                    Write-Verbose "Working in path $Path at depth $depth"
                    Write-Debug "Recurse Object called with PSBoundParameters:`n$($PSBoundParameters | Out-String)"
                    $Depth++

                #Exclude default props if specified, and anything the user specified.
                    $ExcludeProps = @( Get-Exclude $object )

                #Get the children we care about, and their names
                    $Children = $object.psobject.properties | Where {$ExcludeProps -notcontains $_.Name }
                    Write-Debug "Working on properties:`n$($Children | select -ExpandProperty Name | Out-String)"

                #Loop through the children properties.
                foreach($Child in @($Children))
                {
                    $ChildName = $Child.Name
                    $ChildValue = $Child.Value

                    Write-Debug "Working on property $ChildName with value $($ChildValue | Out-String)"
                    # Handle special characters...
                        if($ChildName -match '[^a-zA-Z0-9_]')
                        {
                            $FriendlyChildName = "'$ChildName'"
                        }
                        else
                        {
                            $FriendlyChildName = $ChildName
                        }

                    #Add the property.
                        if((IsIn-Include $ChildName) -and (IsIn-Value $ChildValue) -and $Depth -le $MaxDepth)
                        {
                            $ThisPath = @( $Path + $FriendlyChildName ) -join "."
                            $Output | Add-Member -MemberType NoteProperty -Name $ThisPath -Value $ChildValue
                            Write-Verbose "Adding member '$ThisPath'"
                        }

                    #Handle null...
                        if($ChildValue -eq $null)
                        {
                            Write-Verbose "Skipping NULL $ChildName"
                            continue
                        }

                    #Handle evil looping.  Will likely need to expand this.  Any thoughts on a better approach?
                        if(
                            (
                                $ChildValue.GetType() -eq $Object.GetType() -and
                                $ChildValue -is [datetime]
                            ) -or
                            (
                                $ChildName -eq "SyncRoot" -and
                                -not $ChildValue
                            )
                        )
                        {
                            Write-Verbose "Skipping $ChildName with type $($ChildValue.GetType().fullname)"
                            continue
                        }

                     #Check for arrays by checking object type (this is a fix for arrays with 1 object) otherwise check the count of objects
                        if (($ChildValue.GetType()).basetype.Name -eq "Array") {
                            $IsArray = $true
                        }
                        else {
                            $IsArray = @($ChildValue).count -gt 1
                        }

                        $count = 0

                    #Set up the path to this node and the data...
                        $CurrentPath = @( $Path + $FriendlyChildName ) -join "."

                    #Exclude default props if specified, and anything the user specified.
                        $ExcludeProps = @( Get-Exclude $ChildValue )

                    #Get the children's children we care about, and their names.  Also look for signs of a hashtable like type
                        $ChildrensChildren = $ChildValue.psobject.properties | Where {$ExcludeProps -notcontains $_.Name }
                        $HashKeys = if($ChildValue.Keys -notlike $null -and $ChildValue.Values)
                        {
                            $ChildValue.Keys
                        }
                        else
                        {
                            $null
                        }
                        Write-Debug "Found children's children $($ChildrensChildren | select -ExpandProperty Name | Out-String)"

                    #If we aren't at max depth or a leaf...
                    if(
                        (@($ChildrensChildren).count -ne 0 -or $HashKeys) -and
                        $Depth -lt $MaxDepth
                    )
                    {
                        #This handles hashtables.  But it won't recurse...
                            if($HashKeys)
                            {
                                Write-Verbose "Working on hashtable $CurrentPath"
                                foreach($key in $HashKeys)
                                {
                                    Write-Verbose "Adding value from hashtable $CurrentPath['$key']"
                                    $Output | Add-Member -MemberType NoteProperty -name "$CurrentPath['$key']" -value $ChildValue["$key"]
                                    $Output = Recurse-Object -Object $ChildValue["$key"] -Path "$CurrentPath['$key']" -Output $Output -depth $depth
                                }
                            }
                        #Sub children?  Recurse!
                            else
                            {
                                if($IsArray)
                                {
                                    foreach($item in @($ChildValue))
                                    {
                                        Write-Verbose "Recursing through array node '$CurrentPath'"
                                        $Output = Recurse-Object -Object $item -Path "$CurrentPath[$count]" -Output $Output -depth $depth
                                        $Count++
                                    }
                                }
                                else
                                {
                                    Write-Verbose "Recursing through node '$CurrentPath'"
                                    $Output = Recurse-Object -Object $ChildValue -Path $CurrentPath -Output $Output -depth $depth
                                }
                            }
                        }
                    }

                $Output
            }

        #endregion FUNCTIONS
    }
    Process
    {
        Foreach($Object in $InputObject)
        {
            #Flatten the XML and write it to the pipeline
                Recurse-Object -Object $Object -Output $( New-Object -TypeName PSObject )
        }
    }
}

function Call-FortiOsApi {
    Param (
        $IP,
        $Port = 443,
        $Method, #ValidateSet
        $Path,
        $Name,
        $Mkey,
        $Params = @{} #more efficient alternative when dealing with repetitive parameters
    )

    switch ($Method) {
        "GET" {

        }
        "PUT" {
            #call ccsrftoken
        }
        "POST" {

        }

    }
}
function Create-FortiOSJson {
    Param ( $InputObject )

    $Json = @{
        "json" = $InputObject;
    } | ConvertTo-Json -Compress -Depth 5

    return $Json
}

Function ConvertTo-EC2Filter {
    [CmdletBinding()]
    Param (
        [Parameter (ValueFromPipeline, ValueFromPipelineByPropertyName)]
        [HashTable]$Filter
    )

    Begin { $ec2Filter = @() }
    Process {
        $ec2Filter = Foreach ($key in $Filter.Keys) {
            @{
                name   = $key
                values = $Filter[$key]
            }
        }
    }
    End { $ec2Filter }
}

function Get-CIDR {
    [CmdletBinding()]
    Param
    (
        [Parameter(Mandatory=$false)]
        [ValidateNotNullOrEmpty()]
        [string]$BaseCidr,

        [Parameter(Mandatory=$false)]
        [int]$ChildCidr,

        [Parameter(Mandatory=$false)]
        [int]$Quantity
    )
    

    [string]$BaseCidrHost = $BaseCidr.Split("/")[0]
    [int]$BaseCidrNet = $BaseCidr.Split("/")[1]
    [int]$ChildCidrSize = [Math]::Pow(2,$ChildCidr - $BaseCidrNet)
    [int]$Index = 0

    while ( ($BaseCidrNet = $BaseCidrNet - 8) -ge 0) {
        $Index++
    }

    Write-Log "Index: $Index"
    $Subnet = $BaseCidrHost.Split(".")
    [int]$BaseOctet = $Subnet[$Index]

    if ($Quantity -gt 0) {
        [int]$CeilingOctet = $BaseOctet + $Quantity
    }
    else {
        [int]$CeilingOctet = $BaseOctet + $ChildCidrSize
    }

    Write-Log "Base: $BaseCidr, child: $ChildCidr, qty: $Quantity, childcidrsize: $ChildCidrSize, baseoctet: $BaseOctet, ceilingoctet: $CeilingOctet"
    $RegionVpcCidrs = @()

    while ($BaseOctet -lt $CeilingOctet) {
        [string]$RegionVpcCIDR = $null
        for ($i = 0; $i -lt $Subnet.Count; $i++) {
            if ($i -ne $Index) {
                if ($i -ne ($Subnet.Count - 1) ) {
                    $RegionVpcCidr = $RegionVpcCidr + $Subnet[$i] + "."
                }
                else {
                    $RegionVpcCidr = $RegionVpcCidr + $Subnet[$i] + "/" + $ChildCidr
                }
            }
            else {
                $RegionVpcCidr = $RegionVpcCidr + $BaseOctet + "."
            }
        }
        Write-Log "Region CIDR: $RegionVpcCidr"
        $RegionVpcCidrs += $RegionVpcCidr
        $BaseOctet++
    }
    return $RegionVpcCidrs
}

function Remove-DefaultVpc {
    $DefaultVpcs = Get-Ec2Vpc -Region $Region -Filter @{Name="isDefault";Values="true"}
    if ($DefaultVpcs) {
        foreach ($DefaultVpc in $DefaultVpcs) {
            Write-Log "$($Region): $($DefaultVpc.CidrBlock) is a default VPC."
            Write-Log "Removing $($DefaultVpc.VpcId)"
            try {
                Remove-Ec2Vpc -VpcId $DefaultVpc.VpcId -Region $Region -Force -WhatIf
            }
            catch {
                Get-Exception $_ -Message "Unable to remove $($DefaultVpc.VpcId) in $Region."
            }
        }
    }
    else {
        Write-Log "No default VPC is found in $Region."
    }
}

function Get-CurrentCidrs {
    [CmdletBinding()]
    Param
    (
        [Parameter(Mandatory=$false)]
        [ValidateNotNullOrEmpty()]
        $Regions,

        [Parameter(Mandatory=$false)]
        [ValidateNotNullOrEmpty()]
        $ExcludedRegions        
    )
    
    $CurrentCidrs = @()
    foreach ($Region in $Regions) {
        if ($ExcludedRegions -NotContains $Region) {
            foreach ($CidrBlock in (Get-Ec2Vpc -Region $Region).CidrBlock) {
                $CurrentCidrs += $CidrBlock
            }
        }
    }
    $CurrentCidrs = $CurrentCidrs | Select-Object -Unique
    return $CurrentCidrs
}

function Create-NewVpcs {
    Param
    (
        [Parameter(Mandatory=$false)]
        [ValidateNotNullOrEmpty()]
        $Regions,

        [Parameter(Mandatory=$false)]
        #[ValidateNotNullOrEmpty()]
        $RegionVpcCidrs
    )
    
    $CreatedVpcs = @()
    foreach ($Cidr in $global:CurrentCidrs) {
        $RegionVpcCidrs = $RegionVpcCidrs | Where-Object {$_ -NotMatch $Cidr}
    }

    foreach ($Region in $Regions) {
        while ($true) {
            if ($RegionVpcCidrs) {
                $ProposedVpcCidr = Get-Random -InputObject $RegionVpcCidrs
                if ($ProposedVpcCidr) {
                    Write-Log "Proposed $ProposedVpcCidr CIDR in $Region"
                    Write-Log "Creating new VPC $ProposedVpcCidr in $Region"
                    try {
                        $CreatedVpc = New-Ec2Vpc -CidrBlock $ProposedVpcCidr -Region $Region
                        $CreatedVpcs += @{Vpc = $CreatedVpc; Region = $Region}
                    }
                    catch {
                        Get-Exception $_ -Message "Unable to create new VPC in $Region with $ProposedVpcCidr CIDR."
                    }
                    if (-Not $error) {
                        $RegionVpcCidrs = $RegionVpcCidrs | Where-Object {$_ -NotMatch $ProposedVpcCidr}
                    }
                    Break  
                }
            }
            else {
                Write-Log "No free CIDR available."
                Break
            }
        }
    }
    return $CreatedVpcs
}

function CleanUp {
    Param
    (
        [Parameter(Mandatory=$false)]
        [ValidateNotNullOrEmpty()]
        $FilteredAwsRegions
    )

    Write-Log "Cleaning up..."
    foreach ($Region in $FilteredAwsRegions) {
        Write-Log "Processing $Region region..."
        $Igws = Get-Ec2InternetGateway -Region $Region
        foreach ($Igw in $Igws) {
            foreach ($Vpc in $Igw.Attachments) {
                Write-Log "Dismounting $($Igw.InternetGatewayId) from $($Vpc.VpcId)..."
                Dismount-Ec2InternetGateway -InternetGatewayId $Igw.InternetGatewayId -VpcId $Vpc.VpcId -Region $Region
                Write-Log "Removing $($Igw.InternetGatewayId)..."
                Remove-Ec2InternetGateway -InternetGatewayId $Igw.InternetGatewayId -Region $Region -Force
            }
        }

        $SGs = Get-Ec2SecurityGroup -Region $Region | Where-Object {$_.GroupName -NotMatch "default"}
        foreach ($Sg in $SGs) {
            Write-Log "Removing $($SG.GroupName) $($SG.GroupId) in $($Vpc.VpcId)..."
            try {
                Remove-Ec2Securitygroup -GroupId $SG.GroupId -Region $Region -Force
            }
            catch {
                Get-Exception $_ "Unable to remove $($SG.GroupId) $($SG.GroupName) in $($Vpc.VpcId)."
            }
        }

        $Subnets = Get-Ec2Subnet -Region $Region
        foreach ($Subnet in $Subnets) {
            Write-Log "Removing $($Subnet.SubnetId)..."
            Remove-Ec2Subnet -SubnetId $Subnet.SubnetId -Region $Region -Force
        }

        try {
            $FailedCreations = Get-Ec2Vpc -Region $Region | Where-Object {$_.CidrBlock -NotMatch "172.31.0.0/16"}
        }
        catch {
            Get-Exception $_ "Unable to get list of VPCs."
        }
    
        foreach ($Vpc in $FailedCreations) {
            Write-Log "Removing $Region $($Vpc.VpcId) $($Vpc.CidrBlock)"
            try {
                Remove-Ec2Vpc -VpcId $Vpc.VpcId -Region $Region -Force
            }
            catch {
                Get-Exception $_ "Unable to remove $($Vpc.VpcId) in $Region."
            }
        }
    }
}

function CreateInternetGateway {
    Param
    (
        [Parameter(Mandatory=$false)]
        [ValidateNotNullOrEmpty()]
        $CreatedVpc
    )

    Write-Log "Creating a new internet gateway on $($CreatedVpc.Vpc.VpcId)..."
    try {
        $CreatedIgw = New-Ec2InternetGateway -Region $CreatedVpc.Region
    }
    catch {
        Get-Exception $_ "Unable to created internet gateway on $($CreatedVpc.Vpc.VpcId)."
    }

    if ($CreatedIgw) {
        Write-Log "Attaching $($CreatedIgw.InternetGatewayId) to $($CreatedVpc.Vpc.VpcId)..."
        try {
            Add-Ec2InternetGateway -VpcId $CreatedVpc.Vpc.VpcId -InternetGatewayId $CreatedIgw.InternetGatewayId -Region $CreatedVpc.Region
        }
        catch {
            Get-Exception $_ "Unable to attach $($CreatedIgw.InternetGatewayId) to $($CreatedVpc.Vpc.VpcId)."
        }
    }

    return $CreatedIgw
}

function CreateSecurityGroup {
    Param
    (
        [Parameter(Mandatory=$false)]
        [ValidateNotNullOrEmpty()]
        $CreatedVpc
    )

    Write-Log "Creating a new security group on $($CreatedVpc.Vpc.VpcId)..."
    $SgId = New-Ec2SecurityGroup -GroupName "allow-all-in-out" -Description "allow-all-in-out" -VpcId $CreatedVpc.Vpc.VpcId -Region $CreatedVpc.Region
    $IpPermission = @{ IpProtocol = -1; FromPort = 0; ToPort = 0; IpRanges = "0.0.0.0/0" }
    Write-Log "Granting all in to $SgId"
    Grant-Ec2SecurityGroupIngress -GroupId $SgId -IpPermission @($IpPermission) -Region $CreatedVpc.Region | Out-Null

    return $SgId
}

function CreateSubnet {
    Param
    (
        [Parameter(Mandatory=$false)]
        [ValidateNotNullOrEmpty()]
        $CreatedVpc
    )

    $CreatedSubnets = @()
    $AZs = Get-Ec2AvailabilityZone -Region $CreatedVpc.Region
    Write-Log "Creating subnets..."
    $AzCidrs = Get-CIDR -BaseCidr $CreatedVpc.Vpc.CidrBlock -ChildCidr $global:config.aws.vpc.vpc_subnet_cidr_network_bits -Quantity ($AZs.Count + 1)

    if ($AzCidrs) {
        [int]$i = 0
        foreach ($Az in $AZs) {
            $ProposedAzCidr = $AzCidrs[0]
            if ($ProposedAzCidr) {
                Write-Log "Proposed $ProposedAzCidr CIDR in $($Az.ZoneName)"
                Write-Log "Creating new subnet $ProposedAzCidr in $($Az.ZoneName)"
                try {
                    $CreatedSubnet = New-Ec2Subnet -VpcId $CreatedVpc.Vpc.VpcId -CidrBlock $ProposedAzCidr -AvailabilityZone $Az.ZoneName -Region $CreatedVpc.Region
                    if ($i -eq 0) {
                        [string]$TagValue = $Az.ZoneId + "-public"
                    }
                    else {
                        [string]$TagValue = $Az.ZoneId + "-private" + "-$i"
                    }

                    New-Ec2Tag -Resource $CreatedSubnet.SubnetId -Tag @{Key="Name";Value=$TagValue} -Region $CreatedVpc.Region
                    $CreatedSubnets += @{Subnet = $CreatedSubnet; Region = $CreatedVpc.Region; Az = $Az}
                }
                catch {
                    Get-Exception $_ -Message "Unable to create new subnet in $($Az.ZoneName)"
                }

                $AzCidrs = $AzCidrs | Where-Object {$_ -NotMatch $ProposedAzCidr}
                $i++
            }
            else {
                Write-Log "No free CIDR available."
                Break
            }
        }
    }

    return $CreatedSubnets
}

function AddRouteTableDefaultRoute {
    Param
    (
        [Parameter(Mandatory=$false)]
        [ValidateNotNullOrEmpty()]
        $CreatedVpc,

        [Parameter(Mandatory=$false)]
        [ValidateNotNullOrEmpty()]
        $Igw
    )

    $RouteTables = Get-Ec2RouteTable -Region $CreatedVpc.Region
    if ($RouteTables) {
        foreach ($Rt in $RouteTables) {
            Write-Log "Processing $($Rt.RouteTableId) in $($CreatedVpc.Vpc.VpcId) $($CreatedVpc.Region)..."
        }
    }
    Write-Log "Adding default route via $($Igw.InternetGatewayId)..."
    New-Ec2Route -RouteTableId $Rt.RouteTableId -DestinationCidrBlock 0.0.0.0/0 -GatewayId $Igw.InternetGatewayId -Region $CreatedVpc.Region
}

function CreateVpcPeering {
    Param
    (
        [Parameter(Mandatory=$false)]
        [ValidateNotNullOrEmpty()]
        $CreatedVpc,

        [Parameter(Mandatory=$false)]
        [ValidateNotNullOrEmpty()]
        $Igw
    )

    New-Ec2VpcPeeringConnection -VpcId asdf -PeerVpcId asdf -PeerRegion $CreatedVpc.Region -Region $CreatedVpc.Region -WhatIf
    Get-Ec2VpcPeeringConnection | Where-Object {$_.Status.Code -match "pending-acceptance"} -Region $CreatedVpc.Region -WhatIf
    Approve-EC2VpcPeeringConnection -Region $CreatedVpc.Region -WhatIf

    <#
    TODO
    1. create all new vpcs
    2. foreach vpc to other vpcs, create new peering connection
    3. foreach vpc, approve peering connection
    4. foreach vpc, add route table thru pcx to other vpcs
    #>
}

<#
    TODO
    1. Create a state file to track creating resources for future destruction, similar to Terraform.
    2. Create restricted management security group.
    3. An option to create NAT gateway and its relevant components for private subnet to access internet.
    4. Enable cloud trail to send management events to S3.
#>